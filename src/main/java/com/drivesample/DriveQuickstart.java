package com.drivesample;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.auth.http.HttpCredentialsAdapter;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;

import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.gson.GsonFactory;

import com.google.api.services.drive.Drive;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.FileList;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;


/* class to demonstarte use of Drive files list API */
public class DriveQuickstart {

    

    public static void main(String[] args) throws IOException, GeneralSecurityException {

        // Application name, can be set to anyting.
        String APP_NAME = "Drive API Java Quickstart";


        // Path the the credentials.json file downloaded from the Google Developers Console
        String CREDENTIAL_PATH = "/credentials.json";

        // List of scopes needed to access the Drive API (see https://developers.google.com/resources/api-libraries/documentation/drive/v3/java/latest/com/google/api/services/drive/DriveScopes.html)
        List<String> SCOPES = new ArrayList<String>(Arrays.asList(DriveScopes.DRIVE));
        

        /**
         * AUTHENTICATION
         */
        // Build service account credential.
        HttpTransport httpTransport = GoogleNetHttpTransport.newTrustedTransport();
        JsonFactory jsonFactory = GsonFactory.getDefaultInstance();

        GoogleCredentials googleCredentials =  GoogleCredentials.fromStream(DriveQuickstart.class.getResourceAsStream(CREDENTIAL_PATH))
                    .createScoped(SCOPES);
            HttpRequestInitializer requestInitializer = new HttpCredentialsAdapter(googleCredentials); 

        // Build a new authorized Drive API client service.
        Drive service = new Drive.Builder(httpTransport, jsonFactory, requestInitializer)
                .setApplicationName(APP_NAME)
                .build();


        /**
         * API CALL
         */

         // Build Drive Api request to list files in shared Drive.
        FileList result = service.files().list()
                .setPageSize(10)
                .setFields("nextPageToken, files(id, name)")
                .setIncludeItemsFromAllDrives(true) // include files from shared drives
                .setSupportsAllDrives(true) // This is required to list files from all shared drives
                .setCorpora("allDrives") // List files from user and shared drives
                .execute();

        // List files.
        List<File> files = result.getFiles();
        if (files == null || files.isEmpty()) {
            System.out.println("No files found.");
        } else {
            System.out.println("Files:");
            for (File file : files) {
                System.out.printf("%s (%s)\n", file.getName(), file.getId());
            }
        }
    }
}