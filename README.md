# Drive API Quickstart - Java

## How to use this sample ?

- Download a credential .json file from https://console.cloud.google.com/iam-admin/serviceaccounts
- Replace the existing file in /src/main/resources/credentials.json with the file downloaded
- Package the app with `mvn clean package assembly:single`
- Launch the app by running `java -cp .\target\drivesample-1.0-SNAPSHOT-jar-with-dependencies.jar com.drivesample.DriveQuickstart`
